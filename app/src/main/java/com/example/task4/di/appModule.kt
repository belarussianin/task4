package com.example.task4.di

import com.example.task4.data.repository.BankRepositoryImpl
import org.koin.dsl.module

val appModule = module {
    //Repository
    single { BankRepositoryImpl(get()) }
}