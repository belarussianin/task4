package com.example.task4.data.remote

import com.example.task4.data.remote.dto.AtmDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryName

interface BankAPI {
    @GET("atm?")
    suspend fun getCityAtms(@Query("city") city: String): List<AtmDto>
}