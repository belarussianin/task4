package com.example.task4.data.repository

import com.example.task4.data.remote.BankAPI
import com.example.task4.data.remote.dto.AtmDto
import com.example.task4.domain.repository.BankRepository

class BankRepositoryImpl(
    private val bankAPI: BankAPI
) : BankRepository {
    override suspend fun getAtmsByCity(city: String): List<AtmDto> {
        return bankAPI.getCityAtms(city)
    }
}